USE [teste]
GO
/****** Object:  Table [dbo].[Empresas]    Script Date: 25/04/2019 00:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](max) NULL,
	[InscricaoEstadual] [nvarchar](max) NULL,
	[CNPJ] [nvarchar](max) NULL,
	[Ativa] [bit] NOT NULL,
	[tipo] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Empresas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 25/04/2019 00:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UsuarioId] [int] IDENTITY(1,1) NOT NULL,
	[Login] [nvarchar](max) NULL,
	[Senha] [nvarchar](max) NULL,
	[Nome] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Usuarios] PRIMARY KEY CLUSTERED 
(
	[UsuarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Empresas] ON 

INSERT [dbo].[Empresas] ([Id], [Nome], [InscricaoEstadual], [CNPJ], [Ativa], [tipo]) VALUES (1, N'empresa Vetta group', N'12345678901234', N'12345678901234', 1, 1)
INSERT [dbo].[Empresas] ([Id], [Nome], [InscricaoEstadual], [CNPJ], [Ativa], [tipo]) VALUES (2, N'empresa vale group', N'343243243245563', N'67467567568', 1, 2)
INSERT [dbo].[Empresas] ([Id], [Nome], [InscricaoEstadual], [CNPJ], [Ativa], [tipo]) VALUES (3, N'empresa usiminas', N'789789879', N'8567856787', 1, 2)
INSERT [dbo].[Empresas] ([Id], [Nome], [InscricaoEstadual], [CNPJ], [Ativa], [tipo]) VALUES (4, N'empresa cruzeiro', N'4r534535432', N'678658567', 1, 3)
SET IDENTITY_INSERT [dbo].[Empresas] OFF
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

INSERT [dbo].[Usuarios] ([UsuarioId], [Login], [Senha], [Nome], [Email]) VALUES (1, N'testeapple@ioasys.com.br', N'12341234', N'ioasys', N'1')
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
ALTER TABLE [dbo].[Empresas] ADD  DEFAULT ((0)) FOR [tipo]
GO
