﻿using OwinSample.App_Start;
using OwinSample.Areas.Repository;
using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace OwinSample
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<IRepositoryEmpresa, EmpresaRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IRepositoryUsuario, UsuarioRepository>(new HierarchicalLifetimeManager());
            //container.RegisterType<IServiceUsuario, UsuarioServices>(new HierarchicalLifetimeManager());


            config.DependencyResolver = new UnityResolver(container);
            // Serviços e configuração da API da Web

            // Rotas da API da Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
