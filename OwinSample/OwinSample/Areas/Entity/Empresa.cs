﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OwinSample.Areas.Entity
{
    [Table("Empresas")]
    public class Empresa
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
        public string InscricaoEstadual { get; set; }
        public string CNPJ { get; set; }
        public bool Ativa { get; set; }
        public int tipo { get; set; }

    }
}