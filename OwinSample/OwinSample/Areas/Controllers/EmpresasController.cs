﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using OwinSample.Areas.Entity;
using OwinSample.Areas.Repository;

namespace OwinSample.Areas.Controllers
{
    [Authorize]
    public class EmpresasController : ApiController
    {

        readonly IRepositoryEmpresa _IRepository;
        //private EmpresaDBcontext db = new EmpresaDBcontext();
        // public EmpresasController() { }
        public EmpresasController(IRepositoryEmpresa IRepository)
        {
            this._IRepository = IRepository;
        }
        /// <summary>
        /// Listagem de todas as empresas
        /// </summary>
        /// <returns>çista de empresa</returns>
        [HttpGet]
        [Route("api/enterprises")]
        public async Task<List<Empresa>> GetEmpresaAsync()
        {
            try
            {
                List<Empresa> empresas = await _IRepository.GetEmpresaAsync();
                return empresas;

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message, ex);
            }

        }
        /// <summary>
        /// Listagem de todas as empresas filtrando por nome e tipo
        /// </summary>
        /// <returns>çista de empresa</returns>
        [HttpGet]
        [Route("api/enterprise")]
        public async Task<List<Empresa>> GetEmpresaAsync([FromUri] string name, int? enterprise_types = 0)
        {
            List<Empresa> empresas = await _IRepository.GetEmpresaFilterAsync(enterprise_types, name);
            return empresas;
        }
        /// <summary>
        /// retorna a uma empresa expecifica
        /// </summary>
        /// <returns>çista de empresa</returns>
        [HttpGet]
        [Route("api/enterprises/{id}")]
        public async Task<IHttpActionResult> GetEmpresaAsync( int id)
        {
            //Empresa empresa = await db.Empresa.FindAsync(id);
            Empresa empresa = await _IRepository.GetAsync(id);

            if (empresa == null)
            {
                return NotFound();
            }

            return Ok(empresa);
        }
    }
}