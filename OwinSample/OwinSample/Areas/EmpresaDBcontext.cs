﻿using System.Data.Entity;
using OwinSample.Areas.Entity;

namespace OwinSample.Areas.Repository
{
    public class EmpresaDBcontext: DbContext
    {
        public EmpresaDBcontext() : base("name=EmpresaDBcontext")
        { }
        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Usuario> usuario { get; set; }

    }
}


