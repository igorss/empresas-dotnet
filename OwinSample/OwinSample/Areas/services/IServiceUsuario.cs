﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OwinSample.Areas.services
{
    public interface IServiceUsuario
    {
        Task<bool> Login(string user, string pass);
    }
}
