﻿using OwinSample.Areas.Entity;
using OwinSample.Areas.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OwinSample.Areas.services
{
    public class UsuarioServices
    {
        //readonly IRepositoryUsuario _IRepositoryUsuario;
        //public UsuarioServices(IRepositoryUsuario IRepositoryUsuario)
        //{
        //    _IRepositoryUsuario = IRepositoryUsuario;
        //}
        public static Usuario Login(string user, string pass)
        {
            UsuarioRepository usuarioRepository = new UsuarioRepository();
            Usuario usuario =  usuarioRepository.GetUsuariosync(user);
            if (usuario == null)
                return null;

            if (usuario.Senha != pass)
            { 
                return null;
            } else
            {
                return usuario;
            }
        }
    }
}