﻿using OwinSample.Areas.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OwinSample.Areas.Repository
{
    public class UsuarioRepository : IRepositoryUsuario
    {
        private EmpresaDBcontext db = new EmpresaDBcontext();

        public Usuario GetUsuariosync(string Login)
        {
            var user = db.usuario.Where(p => p.Login == Login).First();
            return user;
        }
    }
}