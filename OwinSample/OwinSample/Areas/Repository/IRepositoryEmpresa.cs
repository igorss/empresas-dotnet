﻿using OwinSample.Areas.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OwinSample.Areas.Repository
{
    public interface IRepositoryEmpresa
    {
        Task<List<Empresa>> GetEmpresaAsync();
        Task<Empresa> GetAsync(int Id);
        Task<List<Empresa>> GetEmpresaFilterAsync(int? Type, string Name);

    }
}
