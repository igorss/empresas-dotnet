﻿using OwinSample.Areas.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OwinSample.Areas.Repository
{
    public interface IRepositoryUsuario
    {
         Usuario GetUsuariosync(string Login);
    }
}
