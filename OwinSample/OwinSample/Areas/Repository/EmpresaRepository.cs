﻿using OwinSample.Areas.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OwinSample.Areas.Repository
{
    public class EmpresaRepository: IRepositoryEmpresa
    {
        private EmpresaDBcontext db = new EmpresaDBcontext();

        public async Task<Empresa> GetAsync(int Id)
        {
            try
            {
                Empresa empresa = await db.Empresa.FindAsync(Id);
                return empresa;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
     
        public async Task<List<Empresa>> GetEmpresaAsync()
        {
            try
            {
                List<Empresa> empresa = await db.Empresa.ToListAsync();

                return empresa;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public async Task<List<Empresa>> GetEmpresaFilterAsync(int? Type, string Name)
        {
            try
            {
                 List<Empresa> empresa = new List<Empresa>();
                if (Type != 0)
                { 
                     empresa = await db.Empresa.Where(p => p.Nome.Contains(Name) && p.tipo == Type).ToListAsync();
                }
                else
                {
                     empresa = await db.Empresa.Where(p => p.Nome.Contains(Name)).ToListAsync();
                }
                return empresa;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}